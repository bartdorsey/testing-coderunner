# Challenge: School Grade Script Analysis

At your company you have an internal training system. It produces grades when
students in the class take quizzes, these grades can be downloaded as a CSV
file.

The file has two columns, "Name" and "Grade", representing the student's name
and their grade respectively. The grade is recorded as a decimal number.

A script was created to average the grades. Your job is to review this script
and create a short explanation of about 5 minutes explaining how it works.

> Note: Inside the file we have including a few variants of the code. You can
> practice by explaining each variant.

You can run the script with the command `npm start`. This will build and run the
`index.ts` file, which imports the `main` function from `student-grades.ts`.

The input file it reads from is named `grades.csv`.
